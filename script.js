// console.log("Hello World!");
// ================= JSON Object
/* - JSON = Javascript Object notation
	- use to read data from web server and display data in a web page
*/

// //JSon Object
// {
// 	"city": "Quezon City",
// 	"province": "Metro Manila",
// 	"country": "Philippines"
// }

// //================JSON ARRAY
// "cities":[
// 	{"city": "Quezon City", "province": "Metro Manila","country": "Philippines"}
// 	{"city": "Pasig City", "province": "Metro Manila","country": "Philippines"}
// 	{"city": "Navotas City", "province": "Metro Manila","country": "Philippines"}
// ]

//===================JSON Methods > JSON.stringify
// JSON.stringify - converts Javascript Object/Array into string.

let batchesArr = [
	{batchName: 'Batch 177'},
	{batchName: 'Batch 178'},
	{batchName: 'Batch 179'}
];
console.log(batchesArr);

let batchArrCont = JSON.stringify(batchesArr);
console.log(`Result from stringify methods:` );
console.log(`Typeof: `,typeof batchArrCont); // data type> typeof
console.log(batchArrCont);

let data = JSON.stringify({
	name: 'John',
	age: 31,
	address: {
		city: "Manila",
		country: "Philippines"
	}
});
let exammple = `{"name":"John",
	"age":31,"address":{"city":"Manila",
	"country":"Philippines"}
}`;
console.log(`example ito `, JSON.parse(exammple));


console.log(data);
//TERMINAL :  npm init -y

//===================JSON Methods > JSON.parse
//para-maaccess ang data 

console.log(`Result from parse method:`);
let dataParse = JSON.parse(data);
console.log(dataParse);
console.log(dataParse.name);
console.log(JSON.parse(batchArrCont));



































